public class SUV extends Car{
	public String carFunction = "OffRoad";
	
	public SUV(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
		
	}
	public String getCarFunction(){
		return carFunction;
	}
}