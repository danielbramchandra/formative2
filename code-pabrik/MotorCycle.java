public class MotorCycle extends Factory{
	public int wheels = 2;
	public String vehicleName;
	public String vehicleColor;
	public long vehiclePrice;
	public String currency;
	public MotorCycle(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		this.vehicleName=vehicleName;
		this.vehicleColor=vehicleColor;
		this.vehiclePrice=vehiclePrice;
		this.currency=currency;
	}
	public int getWheels(){
		return wheels;
	}
	public String getVehicleName(){
		return vehicleName;
	}
	public String getVehicleColor(){
		return vehicleColor;
	}
	public long getVehiclePrice(){
		return vehiclePrice;
	}
	public String getCurrency(){
		return currency;
	}
}