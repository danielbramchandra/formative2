import java.util.LinkedList;
import java.util.Iterator;
import java.text.NumberFormat;
import java.util.Locale;
public class Test{
	public static void main(String[] args) {
		Factory honda = new Factory("HONDA Motors","Serpong");
		MotorCycle cbr = new Sport("CBR150","Black",50_000_000l,"Rp");
		MotorCycle supra = new Bebek("Supra","Blue",23_000_000l,"Rp");
		MotorCycle beat = new Matic("Beat","White",50_000_000l,"Rp");
		Car city = new CityCar("City","Black",19878,"$");
		Car accord = new Sedan("Accord","Blue",50625,"$");
		Car brv = new SUV("BRV","White",16233,"$");
		LinkedList<MotorCycle> listMotor = new LinkedList<MotorCycle>();
		LinkedList<Car> listCar = new LinkedList<Car>();
		listMotor.add(cbr);
		listMotor.add(supra);
		listMotor.add(beat);
		listCar.add(city);
		listCar.add(accord);
		listCar.add(brv);
		Iterator<MotorCycle> itrMotor = listMotor.iterator();
		Iterator<Car> itrCar = listCar.iterator();
		
		System.out.println(honda.getName()+" Factory");
		System.out.println("Address = "+honda.getAddress());
		System.out.println();
		System.out.println("Product");
		System.out.println("MotorCycle");
		while (itrMotor.hasNext()) {
			MotorCycle mc = itrMotor.next();
			if(mc instanceof Sport){
				Sport sport = (Sport)mc;
				System.out.println("Sport");
				System.out.println("\tMax Speed: "+sport.getMaxSpeed());
			}
			if(mc instanceof Bebek){
				Bebek bb = (Bebek)mc;
				System.out.println("Bebek");
				System.out.println("\tMax Speed: "+bb.getMaxSpeed());
			}
			if(mc instanceof Matic){
				Matic matic = (Matic)mc;
				System.out.println("Matic");
				System.out.println("\tMax Speed: "+matic.getMaxSpeed());
			}
			System.out.println("\tName\t : "+mc.getVehicleName());
			System.out.println("\tColour\t : "+mc.getVehicleColor());
			System.out.println("\tPrice\t : "+mc.getCurrency()+mc.getVehiclePrice());
		}
		System.out.println();
		System.out.println("Car");
		while (itrCar.hasNext()) {
			Car mc = itrCar.next();
			if(mc instanceof CityCar){
				CityCar sport = (CityCar)mc;
				System.out.println("CityCar");
				System.out.println("\tFunction : "+sport.getCarFunction());
			}
			if(mc instanceof Sedan){
				Sedan bb = (Sedan)mc;
				System.out.println("Sedan");
				System.out.println("\tFunction : "+bb.getCarFunction());
			}
			if(mc instanceof SUV){
				SUV matic = (SUV)mc;
				System.out.println("SUV");
				System.out.println("\tFunction : "+matic.getCarFunction());
			}
			System.out.println("\tName\t : "+mc.getVehicleName());
			System.out.println("\tColour\t : "+mc.getVehicleColor());
			System.out.println("\tPrice\t : "+mc.getCurrency()+mc.getVehiclePrice());
		}
	}
}