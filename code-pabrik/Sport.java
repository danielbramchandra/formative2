public class Sport extends MotorCycle{
	public String maxSpeed = "200Km/h";
	public Sport(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
	}
	public String getMaxSpeed(){
		return maxSpeed;
	}
}