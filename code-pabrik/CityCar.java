public class CityCar extends Car{
	public String carFunction = "DailyCar";
	public CityCar(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
	}
	public String getCarFunction(){
		return carFunction;
	}
}