public class Sedan extends Car{
	public String carFunction = "FamilyCar";
	public Sedan(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
	}
	public String getCarFunction(){
		return carFunction;
	}
}