public class Bebek extends MotorCycle{
	public String maxSpeed = "120Km/h";

	public Bebek(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
	}
	public String getMaxSpeed(){
		return maxSpeed;
	}

}