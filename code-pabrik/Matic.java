public class Matic extends MotorCycle{
	public String maxSpeed = "80Km/h";
	public Matic(String vehicleName,String vehicleColor,long vehiclePrice,String currency){
		super(vehicleName,vehicleColor,vehiclePrice,currency);
	}
	public String getMaxSpeed(){
		return maxSpeed;
	}
}